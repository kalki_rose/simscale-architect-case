var express = require("express");
var app = express();

//Creating Router() object

var router = express.Router();

var http = require('http').Server(app);
var path = require('path');
var fs = require('fs');


var clientPath = '/../../tmp';
var basePath = '/../../';
var assetPath = clientPath + '/assets';

router.get('/', function(req, res){
    res.sendFile(path.join(__dirname + clientPath + '/index.html'));
});

app.use('/assets', express.static(__dirname + assetPath ));

app.use("/", router);

var port = process.env.VCAP_APP_PORT || 1337;

app.listen(port, function(){
  console.log('listening on 127.0.0.1:1337');
});
