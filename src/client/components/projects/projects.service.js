angular.module('simscale-architecture-case.projects', [])
.service('ProjectsService', [ '$http', function($http) {
    var service = {};

    service.projects = [];
    service.projectsUrl = 'https://www.simscale.com/api/v1/projects';

    service.config = {
        params: {
            limit: 1
        }
    };

    service.getProjects = function() {
        return $http.get(service.projectsUrl, service.config).success( function(response) {
            service.projects = response;
            console.log(response.length);
        });
    };

    service.getProject = function() {
        if (service.projects.length > 0) {
            return service.projects[0];
        }
    };

    return service;
}]);


