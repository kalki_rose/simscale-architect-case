angular.module('simscale-architecture-case.projectCard', [])
.directive('sacProjectCard', [function() {
    return {
        templateUrl: 'project-card/project-card.tpl.html',
        restrict: 'E',
        replace: true,
        scope: {
        	project: '='
        },
        controller: function($scope) {
           console.log($scope.project);
        }
    };
}]);