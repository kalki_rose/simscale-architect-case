angular.module('simscale-architecture-case', [
    'simscale-architecture-case.templatesApp',
    'simscale-architecture-case.header',
    'simscale-architecture-case.projects',
    'simscale-architecture-case.projectCard'
])

.controller( 'AppCtrl', ['ProjectsService', '$scope', function(ProjectsService, $scope) {
	var view = $scope;

	ProjectsService.getProjects().then(function() {
		view.project = ProjectsService.getProject();
		console.log('Project returned', view.project);
	});
}]);

// Bootstrap Angular with Sever Config
angular.element(document).ready(function () {
    angular.bootstrap(document, ['simscale-architecture-case']);
});
